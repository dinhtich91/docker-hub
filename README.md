# docker-lamp

Docker example with Apache, MySql 5.6, PhpMyAdmin, Php and Composer

I use docker-compose as an orchestrator. To run these containers:

```
docker-compose up -d
```

Open phpmyadmin at [http://localhost](http://localhost)
Open web browser to look at a simple php example at [http://localhost:81](http://localhost:81)

Run mysql client:

- `docker-compose exec db mysql -u root -p`

Enjoy !
